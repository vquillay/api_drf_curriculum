from ckeditor.fields import RichTextField
from django.db import models

from users.models import User


class Experience(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='experience')
    date_ini = models.DateTimeField()
    date_end = models.DateTimeField(null=True)
    company = models.CharField(max_length=50)
    description = RichTextField()

    class Meta:
        verbose_name = "Experience"
        verbose_name_plural = "Experiences"

    def __str__(self):
        return f'{self.user.first_name} {self.user.last_name} | {self.company}'
