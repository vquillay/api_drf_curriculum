from django.apps import AppConfig


class ExperienciesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'experiences'
