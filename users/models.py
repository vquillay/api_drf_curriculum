from django.db import models
from django.contrib.auth.models import AbstractUser
from ckeditor.fields import RichTextField


class User(AbstractUser):
    email = models.EmailField('email address', unique=True)
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    modified = models.DateTimeField(auto_now=True)
    photo = models.ImageField(null=True, upload_to='users')
    extract = RichTextField(null=True)
    phone = models.CharField(null=True, max_length=50)
    city = models.CharField(null=True, max_length=50)
    country = models.CharField(null=True, max_length=50)
    is_recruiter = models.BooleanField(default=False)

    class Meta:
        verbose_name = "User"
        verbose_name_plural = "Users"

    def __str__(self):
        return str(f'{self.first_name} {self.first_name}')
