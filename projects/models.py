from ckeditor.fields import RichTextField
from django.db import models

from users.models import User


class Project(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='projects')
    date = models.DateField()
    title = models.CharField(max_length=50)
    url = models.URLField(null=True)
    description= RichTextField()

    class Meta:
        verbose_name = "Project"
        verbose_name_plural = "Projects"

    def __str__(self):
        return f'{self.user.first_name} {self.user.last_name} | {self.company}'
