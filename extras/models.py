from ckeditor.fields import RichTextField
from django.db import models

from users.models import User


class Extra(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='extra_education')
    expedition = models.DateField()
    title = models.CharField(max_length=50)
    url = models.URLField(null=True)
    description = RichTextField(null=True)

    class Meta:
        verbose_name = "s"
        verbose_name_plural = "ss"

    def __str__(self):
        return f'{self.user.first_name} {self.user.last_name} | {self.company}'
