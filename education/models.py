from django.db import models

from users.models import User


class Education(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='education')
    date_ini = models.DateField()
    date_end = models.DateField(null=True)
    title = models.CharField(max_length=50)

    class Meta:
        verbose_name = "s"
        verbose_name_plural = "ss"

    def __str__(self):
        return f'{self.user.first_name} {self.user.last_name} | {self.company}'
